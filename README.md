# vesa-shelf

> Shelf for 8.4mm vesa bracket

<vuepress-open-jscad design="vesa-shelf.jscad" :panel="{size:223}" :camera="{position: {x: 0, y: 0, z: 223},clip: {min: 1, max: 1000}}"></vuepress-open-jscad>

## Running

The jscad project `vesa-shelf` uses gulp to create a `dist/vesaShelf.jscad` file and watches your source for changes. You can drag the `dist/vesaShelf.jscad` directory into the drop area on [openjscad.org](http://openjscad.org). Make sure you check `Auto Reload` and any time you save, gulp creates the `dist/vesaShelf.jscad` file, and your model should refresh.

## jscad-utils

The example project uses [jscad-utils](https://www.npmjs.com/package/jscad-utils). These utilities are a set of utilities that make object creation and alignment easier. To remove it, `npm uninstall --save jscad-utils`, and remove the
`util.init(CSG);` line in `vesaShelf.jscad`.

## Other libraries

You can search [NPM](https://www.npmjs.com/search?q=jscad) for other jscad libraries. Installing them with NPM and running `gulp` should create a `dist/vesaShelf.jscad` will all dependencies injected into the file.

For example, to load a RaspberryPi jscad library and show a Raspberry Pi Model B, install jscad-raspberrypi using `npm install --save jscad-raspberrypi`. Then return a combined `BPlus` group from the `main()` function.

```javascript
main()   util.init(CSG);

  return RaspberryPi.BPlus().combine();
}

### Gist

// include:js
// endinject
```

## License

ISC © [John Cole](http://github.com/)
