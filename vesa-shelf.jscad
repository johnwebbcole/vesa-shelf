// title      : vesaShelf
// author     : John Cole
// license    : ISC License
// file       : vesaShelf.jscad

/* exported main, getParameterDefinitions */

function getParameterDefinitions() {
  return [
    {
      name: "resolution",
      type: "choice",
      values: [0, 1, 2, 3, 4],
      captions: [
        "very low (6,16)",
        "low (8,24)",
        "normal (12,32)",
        "high (24,64)",
        "very high (48,128)"
      ],
      initial: 2,
      caption: "Resolution:"
    }
  ];
}

function main(params) {
  var resolutions = [[6, 16], [8, 24], [12, 32], [24, 64], [48, 128]];
  CSG.defaultResolution3D = resolutions[params.resolution][0];
  CSG.defaultResolution2D = resolutions[params.resolution][1];
  util.init(CSG);

  var shelf = Parts.Cube([98, 98, 5]).Center();
  var a = CAG.roundedRectangle({
    radius: [8.4 + 5, 8.4 + 5],
    roundradius: 2
  });
  var b = CAG.roundedRectangle({
    radius: [8.4 + 25, 8.4 + 25],
    roundradius: 2
  });
  var base = util.poly2solid(b, a, 50).snap(shelf, "z", "outside+");

  var post = Parts.Cube([8.4, 8.4, 20])
    .Center()
    .snap(base, "z", "inside-")
    .color("orange");

  var bollard = Parts.Cylinder(10, 5).bisect("x");

  var left_bollard = bollard.parts.positive
    .snap(shelf, "z", "outside+")
    .snap(shelf, "x", "inside-")
    .color("blue");
  var right_bollard = bollard.parts.negative
    .snap(shelf, "z", "outside+")
    .snap(shelf, "x", "inside+")
    .color("green");

  var woodscrew4 = Hardware.Screw.PanHead(
    ImperialWoodScrews["#4"],
    util.inch(3 / 8),
    null,
    {
      clearLength: 20
    }
  )
    .rotate(shelf, "y", 90)
    .align("thread", post, "xyz");
  // console.log("woodscrew4", woodscrew4);
  return [
    // woodscrew4.combine(),
    shelf,
    left_bollard,
    right_bollard,
    base.subtract([post, woodscrew4.combineAll().enlarge([0.1, 0.1, 0.1])])
  ];
}

// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// endinject
