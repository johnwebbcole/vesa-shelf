// title      : vesaShelf
// author     : John Cole
// license    : ISC License
// file       : vesaShelf.jscad

/* exported main, getParameterDefinitions */

function getParameterDefinitions() {
  return [
    {
      name: "resolution",
      type: "choice",
      values: [0, 1, 2, 3, 4],
      captions: [
        "very low (6,16)",
        "low (8,24)",
        "normal (12,32)",
        "high (24,64)",
        "very high (48,128)"
      ],
      initial: 2,
      caption: "Resolution:"
    }
  ];
}

function main(params) {
  var resolutions = [[6, 16], [8, 24], [12, 32], [24, 64], [48, 128]];
  CSG.defaultResolution3D = resolutions[params.resolution][0];
  CSG.defaultResolution2D = resolutions[params.resolution][1];
  util.init(CSG);

  var shelf = Parts.Cube([98, 98, 5]).Center();
  var a = CAG.roundedRectangle({
    radius: [8.4 + 5, 8.4 + 5],
    roundradius: 2
  });
  var b = CAG.roundedRectangle({
    radius: [8.4 + 25, 8.4 + 25],
    roundradius: 2
  });
  var base = util.poly2solid(b, a, 50).snap(shelf, "z", "outside+");

  var post = Parts.Cube([8.4, 8.4, 20])
    .Center()
    .snap(base, "z", "inside-")
    .color("orange");

  var bollard = Parts.Cylinder(10, 5).bisect("x");

  var left_bollard = bollard.parts.positive
    .snap(shelf, "z", "outside+")
    .snap(shelf, "x", "inside-")
    .color("blue");
  var right_bollard = bollard.parts.negative
    .snap(shelf, "z", "outside+")
    .snap(shelf, "x", "inside+")
    .color("green");

  var woodscrew4 = Hardware.Screw.PanHead(
    ImperialWoodScrews["#4"],
    util.inch(3 / 8),
    null,
    {
      clearLength: 20
    }
  )
    .rotate(shelf, "y", 90)
    .align("thread", post, "xyz");
  // console.log("woodscrew4", woodscrew4);
  return [
    // woodscrew4.combine(),
    shelf,
    left_bollard,
    right_bollard,
    base.subtract([post, woodscrew4.combineAll().enlarge([0.1, 0.1, 0.1])])
  ];
}

// ********************************************************
// Other jscad libraries are injected here.  Do not remove.
// Install jscad libraries using NPM
// ********************************************************
// include:js
// node_modules/jscad-utils/dist/utils.jscad
Boxes={
RabbetJoin:function RabbetJoin(box,thickness,cutHeight,rabbetHeight,cheekGap){
return rabbetJoin(box,thickness,cutHeight,rabbetHeight,cheekGap)},
TopMiddleBottom:function topMiddleBottom(box,thickness){
var bottom=box.bisect("z",thickness)
;var top=bottom.parts.positive.bisect("z",-thickness)
;return util.group("top,middle,bottom",[top.parts.positive,top.parts.negative.color("green"),bottom.parts.negative])
},Rabett:function(box,thickness,gap,height,face){gap=gap||.25
;var inside=-thickness-gap;var outside=-thickness+gap;var group=util.group()
;var top=box.bisect("z",height)
;var bottom=top.parts.negative.bisect("z",height-face)
;group.add(union([top.parts.positive,bottom.parts.positive.subtract(bottom.parts.positive.enlarge(outside,outside,0)).color("green")]),"top")
;group.add(union([bottom.parts.negative,bottom.parts.positive.intersect(bottom.parts.positive.enlarge(inside,inside,0)).color("yellow")]),"bottom")
;return group},RabettTopBottom:function rabbetTMB(box,thickness,gap,options){
options=util.defaults(options,{removableTop:true,removableBottom:true,
topWidth:-thickness,bottomWidth:thickness});gap=gap||.25
;var group=util.group("",{box});var inside=-thickness-gap
;var outside=-thickness+gap;if(options.removableTop){
var top=box.bisect("z",options.topWidth)
;group.add(top.parts.positive.enlarge([inside,inside,0]),"top")
;if(!options.removableBottom)group.add(box.subtract(top.parts.positive.enlarge([outside,outside,0])),"bottom")
}if(options.removableBottom){var bottom=box.bisect("z",options.bottomWidth)
;group.add(bottom.parts.negative.enlarge([outside,outside,0]),"bottomCutout",true)
;group.add(bottom.parts.negative.enlarge([inside,inside,0]),"bottom")
;if(!options.removableTop)group.add(box.subtract(group.parts.bottomCutout),"top")
}if(options.removableBottom&&options.removableTop){
group.add(box.subtract(union([bottom.parts.negative.enlarge([outside,outside,0]),top.parts.positive.enlarge([outside,outside,0])])),"middle")
}return group},CutOut:function cutOut(o,h,box,plug,gap){gap=gap||.25
;var s=o.size();var cutout=o.intersect(box);var cs=o.size()
;var clear=Parts.Cube([s.x,s.y,h]).align(o,"xy").color("yellow")
;var top=clear.snap(o,"z","center+").union(o)
;var back=Parts.Cube([cs.x+6,2,cs.z+2.5]).align(cutout,"x").snap(cutout,"z","center+").snap(cutout,"y","outside-")
;var clip=Parts.Cube([cs.x+2-gap,1-gap,cs.z+2.5]).align(cutout,"x").snap(cutout,"z","center+").snap(cutout,"y","outside-")
;return util.group("insert",{top,bottom:clear.snap(o,"z","center-").union(o),
cutout:union([o,top]),
back:back.subtract(plug).subtract(clip.enlarge(gap,gap,gap)).subtract(clear.translate([0,5,0])),
clip:clip.subtract(plug).color("red"),
insert:union([o,top]).intersect(box).subtract(o).enlarge([-gap,0,0]).union(clip.subtract(plug).enlarge(-gap,-gap,0)).color("blue")
})},Rectangle:function(size,thickness,cb){thickness=thickness||2
;var s=util.array.div(util.xyz2array(size),2);var r=util.array.add(s,thickness)
;var box=CSG.cube({center:r,radius:r}).subtract(CSG.cube({center:r,radius:s}))
;if(cb)box=cb(box);return box},
Hollow:function(object,thickness,interiorcb,exteriorcb){thickness=thickness||2
;var size=-thickness*2;interiorcb=interiorcb||util.identity
;var box=object.subtract(interiorcb(object.enlarge([size,size,size])))
;if(exteriorcb)box=exteriorcb(box);return box},BBox:function(o){
var s=util.array.div(util.xyz2array(o.size()),2);return CSG.cube({center:s,
radius:s}).align(o,"xyz")}};function getRadius(o){
return util.array.div(util.xyz2array(o.size()),2)}
function rabbetJoin(box,thickness,gap,options){options=util.defaults(options,{
removableTop:true,removableBottom:true});gap=gap||.25
;var r=util.array.add(getRadius(box),-thickness/2);r[2]=thickness/2
;var cutter=CSG.cube({center:r,radius:r}).align(box,"xy").color("green")
;var topCutter=cutter.snap(box,"z","inside+");var group=util.group("",{
topCutter,bottomCutter:cutter})
;group.add(box.subtract(cutter.enlarge([gap,gap,0])).color("blue"),"top")
;group.add(box.subtract(topCutter.enlarge([gap,gap,0])).color("red"),"bottom")
;return group}Colors={nameArray:{aliceblue:"#f0f8ff",antiquewhite:"#faebd7",
aqua:"#00ffff",aquamarine:"#7fffd4",azure:"#f0ffff",beige:"#f5f5dc",
bisque:"#ffe4c4",black:"#000000",blanchedalmond:"#ffebcd",blue:"#0000ff",
blueviolet:"#8a2be2",brown:"#a52a2a",burlywood:"#deb887",cadetblue:"#5f9ea0",
chartreuse:"#7fff00",chocolate:"#d2691e",coral:"#ff7f50",
cornflowerblue:"#6495ed",cornsilk:"#fff8dc",crimson:"#dc143c",cyan:"#00ffff",
darkblue:"#00008b",darkcyan:"#008b8b",darkgoldenrod:"#b8860b",
darkgray:"#a9a9a9",darkgrey:"#a9a9a9",darkgreen:"#006400",darkkhaki:"#bdb76b",
darkmagenta:"#8b008b",darkolivegreen:"#556b2f",darkorange:"#ff8c00",
darkorchid:"#9932cc",darkred:"#8b0000",darksalmon:"#e9967a",
darkseagreen:"#8fbc8f",darkslateblue:"#483d8b",darkslategray:"#2f4f4f",
darkslategrey:"#2f4f4f",darkturquoise:"#00ced1",darkviolet:"#9400d3",
deeppink:"#ff1493",deepskyblue:"#00bfff",dimgray:"#696969",dimgrey:"#696969",
dodgerblue:"#1e90ff",firebrick:"#b22222",floralwhite:"#fffaf0",
forestgreen:"#228b22",fuchsia:"#ff00ff",gainsboro:"#dcdcdc",
ghostwhite:"#f8f8ff",gold:"#ffd700",goldenrod:"#daa520",gray:"#808080",
grey:"#808080",green:"#008000",greenyellow:"#adff2f",honeydew:"#f0fff0",
hotpink:"#ff69b4",indianred:"#cd5c5c",indigo:"#4b0082",ivory:"#fffff0",
khaki:"#f0e68c",lavender:"#e6e6fa",lavenderblush:"#fff0f5",lawngreen:"#7cfc00",
lemonchiffon:"#fffacd",lightblue:"#add8e6",lightcoral:"#f08080",
lightcyan:"#e0ffff",lightgoldenrodyellow:"#fafad2",lightgray:"#d3d3d3",
lightgrey:"#d3d3d3",lightgreen:"#90ee90",lightpink:"#ffb6c1",
lightsalmon:"#ffa07a",lightseagreen:"#20b2aa",lightskyblue:"#87cefa",
lightslategray:"#778899",lightslategrey:"#778899",lightsteelblue:"#b0c4de",
lightyellow:"#ffffe0",lime:"#00ff00",limegreen:"#32cd32",linen:"#faf0e6",
magenta:"#ff00ff",maroon:"#800000",mediumaquamarine:"#66cdaa",
mediumblue:"#0000cd",mediumorchid:"#ba55d3",mediumpurple:"#9370d8",
mediumseagreen:"#3cb371",mediumslateblue:"#7b68ee",mediumspringgreen:"#00fa9a",
mediumturquoise:"#48d1cc",mediumvioletred:"#c71585",midnightblue:"#191970",
mintcream:"#f5fffa",mistyrose:"#ffe4e1",moccasin:"#ffe4b5",
navajowhite:"#ffdead",navy:"#000080",oldlace:"#fdf5e6",olive:"#808000",
olivedrab:"#6b8e23",orange:"#ffa500",orangered:"#ff4500",orchid:"#da70d6",
palegoldenrod:"#eee8aa",palegreen:"#98fb98",paleturquoise:"#afeeee",
palevioletred:"#d87093",papayawhip:"#ffefd5",peachpuff:"#ffdab9",peru:"#cd853f",
pink:"#ffc0cb",plum:"#dda0dd",powderblue:"#b0e0e6",purple:"#800080",
red:"#ff0000",rosybrown:"#bc8f8f",royalblue:"#4169e1",saddlebrown:"#8b4513",
salmon:"#fa8072",sandybrown:"#f4a460",seagreen:"#2e8b57",seashell:"#fff5ee",
sienna:"#a0522d",silver:"#c0c0c0",skyblue:"#87ceeb",slateblue:"#6a5acd",
slategray:"#708090",slategrey:"#708090",snow:"#fffafa",springgreen:"#00ff7f",
steelblue:"#4682b4",tan:"#d2b48c",teal:"#008080",thistle:"#d8bfd8",
tomato:"#ff6347",turquoise:"#40e0d0",violet:"#ee82ee",wheat:"#f5deb3",
white:"#ffffff",whitesmoke:"#f5f5f5",yellow:"#ffff00",yellowgreen:"#9acd32"},
name2hex:function(n){n=n.toLowerCase()
;if(!Colors.nameArray[n])return"Invalid Color Name";return Colors.nameArray[n]},
hex2rgb:function(h){h=h.replace(/^\#/,"");if(h.length===6){
return[parseInt(h.substr(0,2),16),parseInt(h.substr(2,2),16),parseInt(h.substr(4,2),16)]
}},_name2rgb:{},name2rgb:function(n){
if(!Colors._name2rgb[n])Colors._name2rgb[n]=this.hex2rgb(this.name2hex(n))
;return Colors._name2rgb[n]},color:function(o,r,g,b,a){
if(typeof r!=="string")return this.setColor(r,g,b,a)
;var c=Colors.name2rgb(r).map(function(x){return x/255});c[3]=g||1
;return o.setColor(c)},init:function init(CSG){var _setColor=CSG.setColor
;CSG.prototype.color=function(r,g,b,a){if(!r)return this
;return Colors.color(this,r,g,b,a)}}};Parts={BBox:function(...objects){
var box=object=>CSG.cube({center:object.centroid(),
radius:object.size().dividedBy(2)});return objects.reduce(function(bbox,part){
var object=bbox?union([bbox,box(part)]):part;return box(object)})},
Cube:function(width){var r=util.divA(util.array.fromxyz(width),2)
;return CSG.cube({center:r,radius:r})},RoundedCube:function(...args){
if(args[0].getBounds){var size=util.size(args[0].getBounds())
;var r=[size.x/2,size.y/2];var thickness=size.z;var corner_radius=args[1]}else{
var r=[args[0]/2,args[1]/2];var thickness=args[2];var corner_radius=args[3]}
var roundedcube=CAG.roundedRectangle({center:[r[0],r[1],0],radius:r,
roundradius:corner_radius}).extrude({offset:[0,0,thickness||1.62]})
;return roundedcube},Cylinder:function(diameter,height,options){
options=util.defaults(options,{start:[0,0,0],end:[0,0,height],radius:diameter/2
});return CSG.cylinder(options)},Cone:function(diameter1,diameter2,height){
return CSG.cylinder({start:[0,0,0],end:[0,0,height],radiusStart:diameter1/2,
radiusEnd:diameter2/2})},Hexagon:function(diameter,height){var radius=diameter/2
;var sqrt3=Math.sqrt(3)/2
;var hex=CAG.fromPoints([[radius,0],[radius/2,radius*sqrt3],[-radius/2,radius*sqrt3],[-radius,0],[-radius/2,-radius*sqrt3],[radius/2,-radius*sqrt3]])
;return hex.extrude({offset:[0,0,height]})},Triangle:function(base,height){
var radius=base/2
;var tri=CAG.fromPoints([[-radius,0],[radius,0],[0,Math.sin(30)*radius]])
;return tri.extrude({offset:[0,0,height]})},
Tube:function Tube(outsideDiameter,insideDiameter,height,outsideOptions,insideOptions){
return Parts.Cylinder(outsideDiameter,height,outsideOptions).subtract(Parts.Cylinder(insideDiameter,height,insideOptions||outsideOptions))
},Board:function(width,height,corner_radius,thickness){
var r=util.divA([width,height],2);var board=CAG.roundedRectangle({
center:[r[0],r[1],0],radius:r,roundradius:corner_radius}).extrude({
offset:[0,0,thickness||1.62]});return board},Hardware:{Orientation:{up:{
head:"outside-",clear:"inside+"},down:{head:"outside+",clear:"inside-"}},
Screw:function(head,thread,headClearSpace,options){
options=util.defaults(options,{orientation:"up",clearance:[0,0,0]})
;var orientation=Parts.Hardware.Orientation[options.orientation]
;var group=util.group("head,thread",{head:head.color("gray"),
thread:thread.snap(head,"z",orientation.head).color("silver")})
;if(headClearSpace){
group.add(headClearSpace.enlarge(options.clearance).snap(head,"z",orientation.clear).color("red"),"headClearSpace",true)
}return group},
PanHeadScrew:function(headDiameter,headLength,diameter,length,clearLength,options){
var head=Parts.Cylinder(headDiameter,headLength)
;var thread=Parts.Cylinder(diameter,length);if(clearLength){
var headClearSpace=Parts.Cylinder(headDiameter,clearLength)}
return Parts.Hardware.Screw(head,thread,headClearSpace,options)},
HexHeadScrew:function(headDiameter,headLength,diameter,length,clearLength,options){
var head=Parts.Hexagon(headDiameter,headLength)
;var thread=Parts.Cylinder(diameter,length);if(clearLength){
var headClearSpace=Parts.Hexagon(headDiameter,clearLength)}
return Parts.Hardware.Screw(head,thread,headClearSpace,options)},
FlatHeadScrew:function(headDiameter,headLength,diameter,length,clearLength,options){
var head=Parts.Cone(headDiameter,diameter,headLength)
;var thread=Parts.Cylinder(diameter,length);if(clearLength){
var headClearSpace=Parts.Cylinder(headDiameter,clearLength)}
return Parts.Hardware.Screw(head,thread,headClearSpace,options)}}};util={
NOZZEL_SIZE:.4,nearest:{
under:function(desired,nozzel=util.NOZZEL_SIZE,nozzie=0){
return(Math.floor(desired/nozzel)+nozzie)*nozzel},
over:function(desired,nozzel=util.NOZZEL_SIZE,nozzie=0){
return(Math.ceil(desired/nozzel)+nozzie)*nozzel}},identity:function(solid){
return solid},result:function(object,f){if(typeof f==="function"){
return f.call(object)}else{return f}},defaults:function(target,defaults){
return Object.assign(defaults,target)},isEmpty:function(variable){
return typeof variable==="undefined"||variable===null},isNegative:function(n){
return((n=+n)||1/n)<0},print:function(msg,o){
echo(msg,JSON.stringify(o.getBounds()),JSON.stringify(this.size(o.getBounds())))
},error:function(msg){if(console&&console.error)console.error(msg)
;throw new Error(msg)},depreciated:function(method,error,message){
var msg=method+" is depreciated."+(" "+message||"")
;if(console&&console.error)console[error?"error":"warn"](msg)
;if(error)throw new Error(msg)},inch:function inch(x){return x*25.4},
cm:function cm(x){return x/25.4},label:function label(text,x,y,width,height){
var l=vector_text(x||0,y||0,text);var o=[];l.forEach(function(pl){
o.push(rectangular_extrude(pl,{w:width||2,h:height||2}))})
;return this.center(union(o))},text:function text(text){
var l=vector_char(0,0,text);var char=l.segments.reduce(function(result,segment){
var path=new CSG.Path2D(segment);var cag=path.expandToCAG(2)
;return result?result.union(cag):cag},undefined);return char},
unitCube:function(length,radius){radius=radius||.5;return CSG.cube({
center:[0,0,0],radius:[radius,radius,length||.5]})},
unitAxis:function(length,radius,centroid){centroid=centroid||[0,0,0]
;return util.unitCube(length,radius).setColor(1,0,0).union([util.unitCube(length,radius).rotateY(90).setColor(0,1,0),util.unitCube(length,radius).rotateX(90).setColor(0,0,1)]).translate(centroid)
},triangle:{toRadians:function toRadians(deg){return deg/180*Math.PI},
toDegrees:function toDegrees(rad){return rad*(180/Math.PI)},
solve:function(p1,p2){var r={c:90,A:Math.abs(p2.x-p1.x),B:Math.abs(p2.y-p1.y)}
;var brad=Math.atan2(r.B,r.A);r.b=util.triangle.toDegrees(brad)
;r.C=r.B/Math.sin(brad);r.a=90-r.b;return r},solve90SA:function(r){
r=Object.assign(r,{C:90});r.A=r.A||90-r.B;r.B=r.B||90-r.A
;var arad=util.triangle.toRadians(r.A)
;r.a=r.a||(r.c?r.c*Math.sin(arad):r.b*Math.tan(arad))
;r.c=r.c||r.a/Math.sin(arad);r.b=r.b||r.a/Math.tan(arad);return r},
centroid(points){var x=points.reduce((sum,point)=>sum+point[0],0)/3
;var y=points.reduce((sum,point)=>sum+point[1],0)/3;return[x,y]}},
toArray:function(a){return Array.isArray(a)?a:[a]},ifArray:function(a,cb){
return Array.isArray(a)?a.map(cb):cb(a)},array:{div:function(a,f){
return a.map(function(e){return e/f})},addValue:function(a,f){
return a.map(function(e){return e+f})},addArray:function(a,f){
return a.map(function(e,i){return e+f[i]})},add:function(a){
return Array.prototype.slice.call(arguments,1).reduce(function(result,arg){
if(Array.isArray(arg)){result=util.array.addArray(result,arg)}else{
result=util.array.addValue(result,arg)}return result},a)},
fromxyz:function(object){
return Array.isArray(object)?object:[object.x,object.y,object.z]},
toxyz:function(a){return{x:a[0],y:a[1],z:a[2]}},first:function(a){
return a?a[0]:undefined},last:function(a){
return a&&a.length>0?a[a.length-1]:undefined},min:function(a){
return a.reduce(function(result,value){return value<result?value:result
},Number.MAX_VALUE)},range:function(a,b){var result=[];for(var i=a;i<b;i++){
result.push(i)}return result}},segment:function(object,segments,axis){
var size=object.size()[axis];var width=size/segments;var result=[]
;for(var i=width;i<size;i+=width){result.push(i)}return result},
zipObject:function(names,values){return names.reduce(function(result,value,idx){
result[value]=values[idx];return result},{})},map:function(o,f){
return Object.keys(o).map(function(key){return f(o[key],key,o)})},
mapValues:function(o,f){return Object.keys(o).map(function(key){
return f(o[key],key)})},pick:function(o,names){
return names.reduce(function(result,name){result[name]=o[name];return result
},{})},mapPick:function(o,names,f,options){
return names.reduce(function(result,name){if(!o[name]){
throw new Error(`${name} not found in ${options.name}: ${Object.keys(o).join(",")}`)
}result.push(f?f(o[name]):o[name]);return result},[])},divA:function divA(a,f){
return this.array.div(a,f)},divxyz:function(size,x,y,z){return{x:size.x/x,
y:size.y/y,z:size.z/z}},div:function(size,d){return this.divxyz(size,d,d,d)},
mulxyz:function(size,x,y,z){return{x:size.x*x,y:size.y*y,z:size.z*z}},
mul:function(size,d){return this.divxyz(size,d,d,d)},
xyz2array:function xyz2array(size){return[size.x,size.y,size.z]},rotationAxes:{
x:[1,0,0],y:[0,1,0],z:[0,0,1]},size:function size(o){
var bbox=o.getBounds?o.getBounds():o;var foo=bbox[1].minus(bbox[0]);return foo},
scale:function scale(size,value){if(value==0)return 1
;return 1+100/(size/value)/100},center:function center(object,size){
size=size||this.size(object.getBounds())
;return this.centerY(this.centerX(object,size),size)},
centerY:function centerY(object,size){size=size||this.size(object.getBounds())
;return object.translate([0,-size.y/2,0])},
centerX:function centerX(object,size){size=size||this.size(object.getBounds())
;return object.translate([-size.x/2,0,0])},
enlarge:function enlarge(object,x,y,z,options={centroid:true}){var a
;if(Array.isArray(x)){a=x}else{a=[x,y,z]}var size=util.size(object)
;var centroid=object.properties&&object.properties.centroid||util.centroid(object,size)
;var idx=0;var t=util.map(size,function(i){return util.scale(i,a[idx++])})
;var new_object=object.translate(centroid.times(-1)).scale(t).translate(centroid)
;if(options.centroid)return new_object
;var new_centroid=new_object.properties&&new_object.properties.centroid||object.properties.centroid||util.centroid(new_object)
;var delta=new_centroid.minus(centroid).times(-1)
;return new_object.translate(delta)},
fit:function fit(object,x,y,z,keep_aspect_ratio){var a;if(Array.isArray(x)){a=x
;keep_aspect_ratio=y;x=a[0];y=a[1];z=a[2]}else{a=[x,y,z]}
var size=this.size(object.getBounds());function scale(size,value){
if(value==0)return 1;return value/size}
var s=[scale(size.x,x),scale(size.y,y),scale(size.z,z)]
;var min=util.array.min(s)
;return util.centerWith(object.scale(s.map(function(d,i){if(a[i]===0)return 1
;return keep_aspect_ratio?min:d})),"xyz",object)},
shift:function shift(object,x,y,z){
var hsize=this.div(this.size(object.getBounds()),2)
;return object.translate(this.xyz2array(this.mulxyz(hsize,x,y,z)))},
zero:function shift(object){var bounds=object.getBounds()
;return object.translate([0,0,-bounds[0].z])},mirrored4:function mirrored4(x){
return x.union([x.mirroredY(90),x.mirroredX(90),x.mirroredY(90).mirroredX(90)])
},flushSide:{"above-outside":[1,0],"above-inside":[1,1],"below-outside":[0,1],
"below-inside":[0,0],"outside+":[0,1],"outside-":[1,0],"inside+":[1,1],
"inside-":[0,0],"center+":[-1,1],"center-":[-1,0]},
calcFlush:function calcFlush(moveobj,withobj,axes,mside,wside){
util.depreciated("calcFlush",false,"Use util.calcSnap instead.");var side
;if(mside===0||mside===1){side=[wside!==undefined?wside:mside,mside]}else{
side=util.flushSide[mside];if(!side)util.error("invalid side: "+mside)}
var m=moveobj.getBounds();var w=withobj.getBounds();if(side[0]===-1){
w[-1]=util.array.toxyz(withobj.centroid())}
return this.axisApply(axes,function(i,axis){
return w[side[0]][axis]-m[side[1]][axis]})},
calcSnap:function calcSnap(moveobj,withobj,axes,orientation,delta){
var side=util.flushSide[orientation];if(!side){var fix={"01":"outside+",
10:"outside-",11:"inside+","00":"inside-","-11":"center+","-10":"center-"}
;util.error("util.calcSnap: invalid side: "+orientation+" should be "+fix[""+orientation+delta])
}var m=moveobj.getBounds();var w=withobj.getBounds();if(side[0]===-1){
w[-1]=withobj.centroid()}var t=this.axisApply(axes,function(i,axis){
return w[side[0]][axis]-m[side[1]][axis]})
;return delta?this.axisApply(axes,function(i){return t[i]+delta}):t},
snap:function snap(moveobj,withobj,axis,orientation,delta){
return moveobj.translate(util.calcSnap(moveobj,withobj,axis,orientation,delta))
},flush:function flush(moveobj,withobj,axis,mside,wside){
return moveobj.translate(util.calcFlush(moveobj,withobj,axis,mside,wside))},
axisApply:function(axes,valfun,a){var retval=a||[0,0,0];var lookup={x:0,y:1,z:2}
;axes.split("").forEach(function(axis){
retval[lookup[axis]]=valfun(lookup[axis],axis)});return retval},
axis2array:function(axes,valfun){util.depreciated("axis2array");var a=[0,0,0]
;var lookup={x:0,y:1,z:2};axes.split("").forEach(function(axis){
var i=lookup[axis];a[i]=valfun(i,axis)});return a},centroid:function(o,size){
var bounds=o.getBounds();size=size||util.size(bounds)
;return bounds[0].plus(size.dividedBy(2))},
calcmidlineTo:function midlineTo(o,axis,to){var bounds=o.getBounds()
;var size=util.size(bounds);return util.axisApply(axis,function(i,a){
return to-size[a]/2})},midlineTo:function midlineTo(o,axis,to){
return o.translate(util.calcmidlineTo(o,axis,to))},
translator:function translator(o,axis,withObj){var centroid=util.centroid(o)
;var withCentroid=util.centroid(withObj);var t=util.axisApply(axis,function(i){
return withCentroid[i]-centroid[i]});return t},
calcCenterWith:function calcCenterWith(o,axes,withObj,delta){
var centroid=util.centroid(o);var withCentroid=util.centroid(withObj)
;var t=util.axisApply(axes,function(i,axis){
return withCentroid[axis]-centroid[axis]})
;return delta?util.array.add(t,delta):t},
centerWith:function centerWith(o,axis,withObj){
return o.translate(util.calcCenterWith(o,axis,withObj))},
group:function group(...args){var self={name:"",names:[],parts:{}}
;if(args&&args.length>0){if(args.length===2){var[names,objects]=args
;self.names=names&&names.length>0&&names.split(",")||[]
;if(Array.isArray(objects)){self.parts=util.zipObject(self.names,objects)
}else if(objects instanceof CSG){self.parts=util.zipObject(self.names,[objects])
}else{self.parts=objects||{}}}else{var[objects]=args
;self.names=Object.keys(objects).filter(k=>k!=="holes")
;self.parts=Object.assign({},objects);self.holes=objects.holes}}
self.map=function(cb){
self.parts=Object.keys(self.parts).filter(k=>k!=="holes").reduce(function(result,key){
result[key]=cb(self.parts[key],key);return result},{});if(self.holes){
if(Array.isArray(self.holes)){self.holes=self.holes.map(function(hole,idx){
return cb(hole,idx)})}else{self.holes=cb(self.holes,"holes")}}return self}
;self.add=function(object,name,hidden,subparts,parts){if(object.parts){if(name){
if(!hidden)self.names.push(name);self.parts[name]=object.combine(parts)
;if(subparts){Object.keys(object.parts).forEach(function(key){
self.parts[subparts+key]=object.parts[key]})}}else{
Object.assign(self.parts,object.parts)
;self.names=self.names.concat(object.names)}}else{
if(!hidden)self.names.push(name);self.parts[name]=object}return self}
;self.clone=function(map){if(!map)map=util.identity;var group=util.group()
;Object.keys(self.parts).forEach(function(key){var part=self.parts[key]
;var hidden=self.names.indexOf(key)==-1
;group.add(map(CSG.fromPolygons(part.toPolygons())),key,hidden)})
;if(self.holes){group.holes=util.toArray(self.holes).map(function(part){
return map(CSG.fromPolygons(part.toPolygons()),"holes")})}return group}
;self.rotate=function(solid,axis,angle){var axes={x:[1,0,0],y:[0,1,0],z:[0,0,1]}
;if(typeof solid==="string"){var _names=solid;solid=self.combine(_names)}
var rotationCenter=solid.centroid();var rotationAxis=axes[axis]
;self.map(function(part){return part.rotate(rotationCenter,rotationAxis,angle)})
;return self};self.combine=function(pieces,options,map){options=Object.assign({
noholes:false},options);pieces=pieces?pieces.split(","):self.names
;if(pieces.length===0){
throw new Error(`no pieces found in ${self.name} pieces: ${pieces} parts: ${Object.keys(self.parts)} names: ${self.names}`)
}var g=union(util.mapPick(self.parts,pieces,function(value,key,object){
return map?map(value,key,object):util.identity(value)},self.name))
;return g.subtractIf(self.holes&&Array.isArray(self.holes)?union(self.holes):self.holes,self.holes&&!options.noholes)
};self.combineAll=function(options,map){
return self.combine(Object.keys(self.parts).join(","),options,map)}
;self.toArray=function(pieces){pieces=pieces?pieces.split(","):self.names
;return pieces.map(function(piece){
if(!self.parts[piece])console.error(`Cannot find ${piece} in ${self.names}`)
;return self.parts[piece]})}
;self.snap=function snap(part,to,axis,orientation,delta){
var t=util.calcSnap(self.combine(part),to,axis,orientation,delta)
;self.map(function(part){return part.translate(t)});return self}
;self.align=function align(part,to,axis,delta){
var t=util.calcCenterWith(self.combine(part,{noholes:true}),axis,to,delta)
;self.map(function(part,name){return part.translate(t)});return self}
;self.midlineTo=function midlineTo(part,axis,to){
var size=self.combine(part).size();var t=util.axisApply(axis,function(i,a){
return to-size[a]/2});self.map(function(part){return part.translate(t)})
;return self};self.translate=function translate(){
var t=Array.prototype.slice.call(arguments,0).reduce(function(result,arg){
result=util.array.addArray(result,arg);return result},[0,0,0])
;self.map(function(part){return part.translate(t)});return self}
;self.pick=function(parts,map){
var p=parts&&parts.length>0&&parts.split(",")||self.names
;if(!map)map=util.identity;var g=util.group();p.forEach(function(name){
g.add(map(CSG.fromPolygons(self.parts[name].toPolygons()),name),name)});return g
};self.array=function(parts,map){
var p=parts&&parts.length>0&&parts.split(",")||self.names
;if(!map)map=util.identity;var a=[];p.forEach(function(name){
a.push(map(CSG.fromPolygons(self.parts[name].toPolygons()),name))});return a}
;return self},getDelta:function getDelta(size,bounds,axis,offset,nonzero){
if(!util.isEmpty(offset)&&nonzero){if(Math.abs(offset)<1e-4){
offset=1e-4*(util.isNegative(offset)?-1:1)}}
var dist=util.isNegative(offset)?offset=size[axis]+offset:offset
;return util.axisApply(axis,function(i,a){
return bounds[0][a]+(util.isEmpty(dist)?size[axis]/2:dist)})},
bisect:function bisect(object,axis,offset,angle,rotateaxis,rotateoffset,options){
options=util.defaults(options,{addRotationCenter:false});angle=angle||0
;var info=util.normalVector(axis);var bounds=object.getBounds()
;var size=util.size(object);rotateaxis=rotateaxis||{x:"y",y:"x",z:"x"}[axis]
;var cutDelta=options.cutDelta||util.getDelta(size,bounds,axis,offset)
;var rotateOffsetAxis={xy:"z",yz:"x",xz:"y"}[[axis,rotateaxis].sort().join("")]
;var centroid=object.centroid()
;var rotateDelta=util.getDelta(size,bounds,rotateOffsetAxis,rotateoffset)
;var rotationCenter=options.rotationCenter||new CSG.Vector3D(util.axisApply("xyz",function(i,a){
if(a==axis)return cutDelta[i];if(a==rotateOffsetAxis)return rotateDelta[i]
;return centroid[a]}));var rotationAxis=util.rotationAxes[rotateaxis]
;var cutplane=CSG.OrthoNormalBasis.GetCartesian(info.orthoNormalCartesian[0],info.orthoNormalCartesian[1]).translate(cutDelta).rotate(rotationCenter,rotationAxis,angle)
;var g=util.group("negative,positive",[object.cutByPlane(cutplane.plane),object.cutByPlane(cutplane.plane.flipped())])
;if(options.addRotationCenter)g.add(util.unitAxis(size.length()+10,.5,rotationCenter),"rotationCenter")
;return g},stretch:function stretch(object,axis,distance,offset){var normal={
x:[1,0,0],y:[0,1,0],z:[0,0,1]};var bounds=object.getBounds()
;var size=util.size(object)
;var cutDelta=util.getDelta(size,bounds,axis,offset,true)
;return object.stretchAtPlane(normal[axis],cutDelta,distance)},
poly2solid:function poly2solid(top,bottom,height){if(top.sides.length==0){
return new CSG}var offsetVector=CSG.Vector3D.Create(0,0,height)
;var normalVector=CSG.Vector3D.Create(0,1,0);var polygons=[]
;polygons=polygons.concat(bottom._toPlanePolygons({translation:[0,0,0],
normalVector,flipped:!(offsetVector.z<0)}))
;polygons=polygons.concat(top._toPlanePolygons({translation:offsetVector,
normalVector,flipped:offsetVector.z<0}))
;var c1=new CSG.Connector(offsetVector.times(0),[0,0,offsetVector.z],normalVector)
;var c2=new CSG.Connector(offsetVector,[0,0,offsetVector.z],normalVector)
;polygons=polygons.concat(bottom._toWallPolygons({cag:top,toConnector1:c1,
toConnector2:c2}));return CSG.fromPolygons(polygons)},
slices2poly:function slices2poly(slices,options,axis){
var twistangle=options&&parseFloat(options.twistangle)||0
;var twiststeps=options&&parseInt(options.twiststeps)||CSG.defaultResolution3D
;if(twistangle==0||twiststeps<1){twiststeps=1}
var normalVector=options.si.normalVector;var polygons=[]
;var first=util.array.first(slices);var last=util.array.last(slices)
;var up=first.offset[axis]>last.offset[axis]
;polygons=polygons.concat(first.poly._toPlanePolygons({translation:first.offset,
normalVector,flipped:!up}));var rotateAxis="rotate"+axis.toUpperCase()
;polygons=polygons.concat(last.poly._toPlanePolygons({translation:last.offset,
normalVector:normalVector[rotateAxis](twistangle),flipped:up}))
;var rotate=twistangle===0?function rotateZero(v){return v
}:function rotate(v,angle,percent){return v[rotateAxis](angle*percent)}
;var connectorAxis=last.offset.minus(first.offset).abs()
;slices.forEach(function(slice,idx){if(idx<slices.length-1){var nextidx=idx+1
;var top=!up?slices[nextidx]:slice;var bottom=up?slices[nextidx]:slice
;var c1=new CSG.Connector(bottom.offset,connectorAxis,rotate(normalVector,twistangle,idx/slices.length))
;var c2=new CSG.Connector(top.offset,connectorAxis,rotate(normalVector,twistangle,nextidx/slices.length))
;polygons=polygons.concat(bottom.poly._toWallPolygons({cag:top.poly,
toConnector1:c1,toConnector2:c2}))}});return CSG.fromPolygons(polygons)},
normalVector:function normalVector(axis){var axisInfo={z:{
orthoNormalCartesian:["X","Y"],normalVector:CSG.Vector3D.Create(0,1,0)},x:{
orthoNormalCartesian:["Y","Z"],normalVector:CSG.Vector3D.Create(0,0,1)},y:{
orthoNormalCartesian:["X","Z"],normalVector:CSG.Vector3D.Create(0,0,1)}}
;if(!axisInfo[axis])util.error("util.normalVector: invalid axis "+axis)
;return axisInfo[axis]},
sliceParams:function sliceParams(orientation,radius,bounds){
var axis=orientation[0];var direction=orientation[1];var dirInfo={"dir+":{
sizeIdx:1,sizeDir:-1,moveDir:-1,positive:true},"dir-":{sizeIdx:0,sizeDir:1,
moveDir:0,positive:false}};var info=dirInfo["dir"+direction]
;return Object.assign({axis,cutDelta:util.axisApply(axis,function(i,a){
return bounds[info.sizeIdx][a]+Math.abs(radius)*info.sizeDir}),
moveDelta:util.axisApply(axis,function(i,a){
return bounds[info.sizeIdx][a]+Math.abs(radius)*info.moveDir})
},info,util.normalVector(axis))},
reShape:function reShape(object,radius,orientation,options,slicer){
options=options||{};var b=object.getBounds();var ar=Math.abs(radius)
;var si=util.sliceParams(orientation,radius,b)
;if(si.axis!=="z")throw new Error('util.reShape error: CAG._toPlanePolytons only uses the "z" axis.  You must use the "z" axis for now.')
;var cutplane=CSG.OrthoNormalBasis.GetCartesian(si.orthoNormalCartesian[0],si.orthoNormalCartesian[1]).translate(si.cutDelta)
;var slice=object.sectionCut(cutplane)
;if(object.properties.centroid)slice.properties={
centroid:new CSG.Vector2D([object.properties.centroid.x,object.properties.centroid.y])
};var first=util.axisApply(si.axis,function(){return si.positive?0:ar})
;var last=util.axisApply(si.axis,function(){return si.positive?ar:0})
;var plane=si.positive?cutplane.plane:cutplane.plane.flipped()
;var slices=slicer(first,last,slice)
;var delta=util.slices2poly(slices,Object.assign(options,{si}),si.axis)
;var remainder=object.cutByPlane(plane)
;return union([options.unionOriginal?object:remainder,delta.translate(si.moveDelta)])
},chamfer:function chamfer(object,radius,orientation,options){
return util.reShape(object,radius,orientation,options,function(first,last,slice){
return[{poly:slice,offset:new CSG.Vector3D(first)},{
poly:util.enlarge(slice,[-radius*2,-radius*2],undefined,undefined,options),
offset:new CSG.Vector3D(last)}]})},
fillet:function fillet(object,radius,orientation,options){options=options||{}
;return util.reShape(object,radius,orientation,options,function(first,last,slice){
var v1=new CSG.Vector3D(first);var v2=new CSG.Vector3D(last)
;var res=options.resolution||CSG.defaultResolution3D
;var slices=util.array.range(0,res).map(function(i){var p=i>0?i/(res-1):0
;var v=v1.lerp(v2,p);var size=-radius*2-Math.cos(Math.asin(p))*(-radius*2)
;return{poly:util.enlarge(slice,[size,size]),offset:v}});return slices})},
calcRotate:function(part,solid,axis,angle){var axes={x:[1,0,0],y:[0,1,0],
z:[0,0,1]};var rotationCenter=solid.centroid();var rotationAxis=axes[axis]
;return{rotationCenter,rotationAxis}},
rotateAround:function(part,solid,axis,angle){
var{rotationCenter,rotationAxis}=util.calcRotate(part,solid,axis,angle)
;return part.rotate(rotationCenter,rotationAxis,angle)},init:function init(CSG){
if(Colors&&Colors.init)Colors.init(CSG)
;CSG.prototype.flush=function flush(to,axis,mside,wside){
return util.flush(this,to,axis,mside,wside)}
;CSG.prototype.snap=function snap(to,axis,orientation,delta){
return util.snap(this,to,axis,orientation,delta)}
;CSG.prototype.calcSnap=function calcSnap(to,axis,orientation,delta){
return util.calcSnap(this,to,axis,orientation,delta)}
;CSG.prototype.midlineTo=function midlineTo(axis,to){
return util.midlineTo(this,axis,to)}
;CSG.prototype.calcmidlineTo=function midlineTo(axis,to){
return util.calcmidlineTo(this,axis,to)}
;CSG.prototype.centerWith=function centerWith(axis,to){
util.depreciated("centerWith",true,"Use align instead.")
;return util.centerWith(this,axis,to)}
;if(CSG.center)echo("CSG already has .center")
;CSG.prototype.center=function center(axis){
return util.centerWith(this,axis||"xyz",util.unitCube(),0)}
;CSG.prototype.calcCenter=function centerWith(axis){
return util.calcCenterWith(this,axis||"xyz",util.unitCube(),0)}
;CSG.prototype.align=function align(to,axis){
return util.centerWith(this,axis,to)}
;CSG.prototype.calcAlign=function calcAlign(to,axis,delta){
return util.calcCenterWith(this,axis,to,delta)}
;CSG.prototype.enlarge=function enlarge(x,y,z){return util.enlarge(this,x,y,z)}
;CAG.prototype.enlarge=function cag_enlarge(x,y){return util.enlarge(this,x,y)}
;CSG.prototype.fit=function fit(x,y,z,a){return util.fit(this,x,y,z,a)}
;if(CSG.size)echo("CSG already has .size");CSG.prototype.size=function(){
return util.size(this.getBounds())};CSG.prototype.centroid=function(){
return util.centroid(this)};CSG.prototype.Zero=function zero(){
return util.zero(this)};CSG.prototype.Center=function Center(axes){
return this.align(util.unitCube(),axes||"xy")}
;CSG.Vector2D.prototype.map=function Vector2D_map(cb){
return new CSG.Vector2D(cb(this.x),cb(this.y))}
;CSG.prototype.fillet=function fillet(radius,orientation,options){
return util.fillet(this,radius,orientation,options)}
;CSG.prototype.chamfer=function chamfer(radius,orientation,options){
return util.chamfer(this,radius,orientation,options)}
;CSG.prototype.bisect=function bisect(axis,offset,angle,rotateaxis,rotateoffset,options){
return util.bisect(this,axis,offset,angle,rotateaxis,rotateoffset,options)}
;CSG.prototype.stretch=function stretch(axis,distance,offset){
return util.stretch(this,axis,distance,offset)}
;CSG.prototype.unionIf=function unionIf(object,condition){
return condition?this.union(util.result(this,object)):this}
;CSG.prototype.subtractIf=function subtractIf(object,condition){
return condition?this.subtract(util.result(this,object)):this}
;CSG.prototype._translate=CSG.prototype.translate
;CSG.prototype.translate=function translate(){if(arguments.length===1){
return this._translate(arguments[0])}else{
var t=Array.prototype.slice.call(arguments,0).reduce(function(result,arg){
result=util.array.addArray(result,arg);return result},[0,0,0])
;return this._translate(t)}}}};
// node_modules/jscad-hardware/dist/jscad-hardware.jscad
ImperialBolts={"1/4 hex":{name:"1/4 hex",E:util.inch(.25),tap:util.inch(.201),
close:util.inch(.257),loose:util.inch(.266),H:util.inch(5/32),G:util.inch(.505),
F:util.inch(7/16),type:"HexHeadScrew"},"1/4 socket":{name:"1/4 socket",
E:util.inch(.25),tap:util.inch(.201),close:util.inch(.257),
loose:util.inch(.266),H:util.inch(.25),D:util.inch(.375),type:"PanHeadScrew"},
"5/16 hex":{name:"5/16 hex",E:util.inch(.3125),tap:util.inch(.257),
close:util.inch(.323),loose:util.inch(.332),H:util.inch(.203125),
G:util.inch(.577),F:util.inch(.5),type:"HexHeadScrew"}};var ImperialWashers={
"1/4":{od:util.inch(.734),id:util.inch(.312),thickness:util.inch(.08)},
"1/4 fender":{od:util.inch(1.25),id:util.inch(.28125),thickness:util.inch(.08)},
"5/16":{od:util.inch(1.25),id:util.inch(.34375),thickness:util.inch(.08)}}
;var ImperialWoodScrews={"#4":[util.inch(.225),util.inch(.067),util.inch(.112)],
"#6":[util.inch(.279),util.inch(.083),util.inch(.138)],
"#8":[util.inch(.332),util.inch(.1),util.inch(.164)],
"#10":[util.inch(.385),util.inch(.116),util.inch(.19)],
"#12":[util.inch(.438),util.inch(.132),util.inch(.216)]};var ImperialNuts={
"1/4 hex":[11.113,12.8,5.56,6.35]};Hardware={
Bolt:function Bolt(length,bolt,fit,BOM){fit=fit||"loose";if(BOM){
var bomkey=`${bolt.name} - ${util.cm(length).toFixed(2)}`
;if(!BOM[bomkey])BOM[bomkey]=0;BOM[bomkey]++}
var b=Parts.Hardware[bolt.type](bolt.G||bolt.D,bolt.H,bolt.E,length)
;var clearance=bolt[fit]-bolt.E
;b.add(Parts.Hardware[bolt.type]((bolt.G||bolt.D)+clearance,bolt.H+clearance,bolt[fit],length,length).map(part=>part.color("red")),"tap",false,"tap-")
;return b},Washer:function Washer(washer,fit){var w=util.group()
;w.add(Parts.Tube(washer.od,washer.id,washer.thickness).color("gray"),"washer")
;if(fit){var tap=Hardware.Clearances[fit]
;if(!tap)console.error(`Hardware.Washer unknown fit clearance ${fit}, should be ${Object.keys(Hardware.Clarances).join("|")}`)
;w.add(Parts.Cylinder(washer.od+Hardware.Clearances[fit],washer.thickness).color("red"),"clearance")
}return w},Nut:function Nut(nut,fit){
return Parts.Hexagon(nut[1]+Hardware.Clearances[fit],nut[2])},Screw:{
PanHead:function(type,length,fit,options={}){
var[headDiameter,headLength,diameter,tap,countersink]=type
;return Hardware.PanHeadScrew(headDiameter,headLength,diameter,length,options.clearLength,options)
},FlatHead:function(type,length,fit,options={}){
var[headDiameter,headLength,diameter,tap,countersink]=type
;return Hardware.FlatHeadScrew(headDiameter,headLength,diameter,length,options.clearLength,options)
}},Clearances:{tap:util.inch(-.049),close:util.inch(.007),loose:util.inch(.016)
},Orientation:{up:{head:"outside-",clear:"inside+"},down:{head:"outside+",
clear:"inside-"}},CreateScrew:function(head,thread,headClearSpace,options){
options=util.defaults(options,{orientation:"up",clearance:[0,0,0]})
;var orientation=Parts.Hardware.Orientation[options.orientation]
;var group=util.group("head,thread",{head:head.color("gray"),
thread:thread.snap(head,"z",orientation.head).color("silver")})
;if(headClearSpace){
group.add(headClearSpace.enlarge(options.clearance).snap(head,"z",orientation.clear).color("red"),"headClearSpace",true)
}return group},
PanHeadScrew:function(headDiameter,headLength,diameter,length,clearLength,options){
var head=Parts.Cylinder(headDiameter,headLength)
;var thread=Parts.Cylinder(diameter,length);if(clearLength){
var headClearSpace=Parts.Cylinder(headDiameter,clearLength)}
return Hardware.CreateScrew(head,thread,headClearSpace,options)},
HexHeadScrew:function(headDiameter,headLength,diameter,length,clearLength,options){
var head=Parts.Hexagon(headDiameter,headLength)
;var thread=Parts.Cylinder(diameter,length);if(clearLength){
var headClearSpace=Parts.Hexagon(headDiameter,clearLength)}
return Hardware.CreateScrew(head,thread,headClearSpace,options)},
FlatHeadScrew:function(headDiameter,headLength,diameter,length,clearLength,options){
var a=headDiameter;var b=diameter;if(options&&options.orientation=="down"){
a=diameter;b=headDiameter}var head=Parts.Cone(a,b,headLength)
;var thread=Parts.Cylinder(diameter,length);if(clearLength){
var headClearSpace=Parts.Cylinder(headDiameter,clearLength)}
return Hardware.CreateScrew(head,thread,headClearSpace,options)}}
;var MetricBolts={"m10 hex":{E:10,tap:8.5,close:10.5,loose:11,H:6.5,G:18,F:17,
type:"HexHeadScrew"},"m12 hex":{E:12,tap:10.3,close:12.6,loose:13.2,H:7.5,G:20,
F:19,type:"HexHeadScrew"}};var MetricScrews={m4:[8,3.1,4]};var MetricNuts={
m4:[7,8.1,3.2,4]};
// endinject
